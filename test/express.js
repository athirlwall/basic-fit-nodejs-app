const request = require('supertest')
const app = require('../server.js');
const requestApp = request(app.app)
const jwt = require('jsonwebtoken');

const VALID_EMAIL = 'alex@alex.com'
const VALID_PASSWORD = '123'
const VALID_USER_NAME = 'Alex'
const INVALID_EMAIL = 'unknown@unknown.com'
const INVALID_PASSWORD = 'XXX'
const INCORRECT_EMAIL = 'Incorrect Email'

function doLogin(done, email, password, httpStatus, jsonResponse) {
    requestApp
    .post('/login')
    .send({
        email,
        password
    })
    .set('Accept', 'application/json')
    .expect(httpStatus, jsonResponse)
    .expect('Content-Type', /json/)
    .end(done)
}

describe('Test Basic Fit API', function () {
  it('responds to /', function testSlash(done) {
    requestApp
    .get('/')
    .expect(app.HTTP_OK, app.SERVER_APP_NAME, done)
  })

  it('respond OK to /login with valid email and password', function(done) {
    doLogin(done, VALID_EMAIL, VALID_PASSWORD, app.HTTP_OK, { userName: 'Alex' })
  })

  it('respond Unauthorized to /login with incorrect email address', function(done) {
    doLogin(done, INCORRECT_EMAIL, VALID_PASSWORD, app.HTTP_UNAUTHORIZED, { error: app.ERROR_INCORRECT_EMAIL })
  })

  it('respond Unauthorized to /login with unknown email', function(done) {
    doLogin(done, INVALID_EMAIL, INVALID_PASSWORD, app.HTTP_UNAUTHORIZED, { error: app.ERROR_UNKNOWN_EMAIL })
  })

  it('respond Unauthorized to /login with incorrect password', function(done) {
    doLogin(done, VALID_EMAIL, INVALID_PASSWORD, app.HTTP_UNAUTHORIZED, { error: app.ERROR_INCORRECT_PASSWORD })
  })

  it('Valid login should send a valid cookie', function(done) {

    function expectCookie(res) {
      if (!('set-cookie' in res.header)) {
        console.log('Cookie Error')
        throw new Error("No cookies were set during login");
      }

      let setCookieHeader = res.header['set-cookie'][0]

      if (!setCookieHeader.includes(`${app.COOKIE_NAME}=`)) {
        console.log('Cookie Error')
        throw new Error("basicFitSession Cookie not set during login");
      }

      const expectedCookie = `${app.COOKIE_NAME}=${token};`

      if (!setCookieHeader.includes(expectedCookie)) {
        console.log('Cookie Error')
        console.log('Received', setCookieHeader)
        console.log('Expected', expectedCookie)
        throw new Error('basicFitSession Cookie not set to expected value');
      }

      if (!setCookieHeader.includes('HttpOnly')) {
        console.log('Cookie Error')
        throw new Error("Returned cookie should be HttpOnly");
      }
    }

    const token = jwt.sign(
      { userName: VALID_USER_NAME },
      app.SECRET,
      { expiresIn: '1h' }
    );

    requestApp
    .post('/login')
    .send({
        email: VALID_EMAIL,
        password: VALID_PASSWORD
    })
    .set('Accept', 'application/json')
    .expect(expectCookie)
    .end(done)
  });
});