
const express = require('express')
const cookieParser = require('cookie-parser')

const app = express()
app.use(express.json())
app.use(cookieParser())
app.use(express.static('public'))

const jwt = require('jsonwebtoken');
const encrypt = require('./encrypt.js')
const db = require('./db.js')
const emailValidator = require("email-validator"); 

const COOKIE_NAME = 'basicFitSession'
const HTTP_OK = 200
const HTTP_UNAUTHORIZED = 401
const ONE_HOUR_MS = 3600000
const SECRET = 'jogw897e4tojf98jowjitg984'
const SERVER_APP_NAME = 'Basic Fit Login API'
const ERROR_INCORRECT_EMAIL = 'Not a valid email address'
const ERROR_UNKNOWN_EMAIL = 'Email address not recognized'
const ERROR_INCORRECT_PASSWORD = 'Incorrect password'

console.log(`Starting ${SERVER_APP_NAME} .... `)

// Sequelize authentication
if (!db.authenticate()) {
    console.error('Aborted startup - could not authenticate DB.')
    process.exit()
}

// Reset DB and populate test records
db.initializeDb()

// Handle CORS restrictions
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*')
    res.header('Access-Control-Allow-Headers', 'Content-Type')
    next();
});

// Test endpoint
app.get('/', function (req, res) {
    res.send(SERVER_APP_NAME)
})

app.post('/login', async (req, res) => {

    if (!emailValidator.validate(req.body.email)) {
        res.status(HTTP_UNAUTHORIZED).json({ error: ERROR_INCORRECT_EMAIL })
        return
    }

    // Locate matching email record
    const matches = await db.User.findAll({
        where: {
          email: req.body.email
        },
        limit: 1
    });

    if (matches.length === 1) {

        // Encrypt submitted password and check against encrypted password in user record
        if (encrypt.sha512(req.body.password, matches[0].salt) === matches[0].encrypted) {
            const userName = matches[0].name

            // Set JSON web token based on authorized user name
            const token = jwt.sign(
                { userName },
                SECRET,
                { expiresIn: '1h' }
            );
            
            // Set http-only cookie with web token
            res.cookie('basicFitSession', token, { 
                maxAge: ONE_HOUR_MS,
                httpOnly: true,
                // When over HTTPS
                // secure: true 
            })

            // Return user info for client application
            // (At the moment, only the user name)
            res.status(HTTP_OK).json({ 
                userName
            })
        } else {

            // Passed and stored passwords do not match
            res.status(HTTP_UNAUTHORIZED).json({ error: ERROR_INCORRECT_PASSWORD })
        }
    } else {

        // Email record not found in user table
        res.status(HTTP_UNAUTHORIZED).json({ error: ERROR_UNKNOWN_EMAIL })
    }
})


// Start server if DB authenticated
const server = app.listen(3001, () => {
    var host = server.address().address
    var port = server.address().port
    console.log(`${SERVER_APP_NAME} listening at http://%s:%s`, host, port)
})

module.exports = {
    app,
    SERVER_APP_NAME,
    HTTP_OK,
    HTTP_UNAUTHORIZED,
    ERROR_UNKNOWN_EMAIL,
    ERROR_INCORRECT_PASSWORD,
    ERROR_INCORRECT_EMAIL,
    SECRET,
    COOKIE_NAME
}