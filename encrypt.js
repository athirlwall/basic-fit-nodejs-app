const crypto = require('crypto');

// Generate random hex string for the 'salt'
function randomHexString(length){
    return crypto
    .randomBytes(Math.ceil(length / 2))
    .toString('hex')
    .slice(0,length);
};

// Use sha512 encryption
function sha512(password, salt){
    return crypto.createHmac('sha512', salt)
    .update(password)
    .digest('hex');
};

// Given a password, return a random salt and an encrypted password
// Will be stored in the DB as a user record
function encrypt(password) {
    const salt = randomHexString(6);
    const encrypted = sha512(password, salt);
    return {
        salt,
        encrypted
    }
}

module.exports = {
    sha512,
    encrypt
}