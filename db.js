const { Sequelize, DataTypes } = require('sequelize');
const sequelize = new Sequelize('sqlite::memory:')
const testData = require('./data.js')

// Sequelize definition of user table
// User table will be created when sequelize.sync called
// (see initializeDb below)
const User = sequelize.define('User', {
    name: {
        type: DataTypes.STRING,
    },
    email: {
        type: DataTypes.STRING
    },
    encrypted: {
        type: DataTypes.STRING
    },
    salt: {
        type: DataTypes.STRING
    }
});

// Authenticate DB and make sure it's ready
async function authenticate() {
    try {
        await sequelize.authenticate();
        console.log('Sequelize authenticated.');
        return true
    } catch (error) {
        console.error('Could not authenticate Sequelize:', error);
        return false
    }
}

// Initialize DB: authenticate and create dummy test records
async function initializeDb() {
    await sequelize.sync({ force: true });

    await User.bulkCreate(testData).then(function() {
        return User.findAll();
    }).then(function(users) {
        console.log('Created users', users)
    })
}

module.exports = {
    authenticate,
    initializeDb,
    User,
}