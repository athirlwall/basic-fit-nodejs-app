const encrypt = require('./encrypt.js')

const data = [
    { name: 'Alex', email: 'alex@alex.com', ...encrypt.encrypt('123') },
    { name: 'Jack', email: 'jack@jack.com', ...encrypt.encrypt('456') },
    { name: 'Eris', email: 'eris@eris.com', ...encrypt.encrypt('789') }
]

module.exports = data